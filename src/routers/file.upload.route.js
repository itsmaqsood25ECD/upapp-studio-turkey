const express = require('express');
const aws = require('aws-sdk');
const multerS3 = require('multer-s3');
const multer = require('multer');
const path = require('path');

const router = express.Router();

const s3 = new aws.S3({
	secretAccessKey: "9FFygEl8SesuToHThZm7xhAmv/YUo5b1O3wOd1aJ",
	accessKeyId: "AKIAXQRXDP3BH5YQO2VP",
	region: "us-east-2",
});

/**
 * File Type Check
 */
function checkFileType(file, cb) {
	// Allowed ext
	const filetypes = /jpeg|jpg|png|gif/;
	// Check ext
	const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
	// Check mime
	const mimetype = filetypes.test(file.mimetype);
	if (mimetype && extname) {
		return cb(null, true);
	} else {
		cb('Error: Images Only!');
	}
}

/**
 * Single feature Img Upload
 */
const featureImgUpload = multer({
	storage: multerS3({
		s3: s3,
		bucket: 'file-uploads-application-management',
		acl: 'public-read',
		key: function (req, file, cb) {
			cb(null, path.basename(file.originalname, path.extname(file.originalname)) + '-' + Date.now() + path.extname(file.originalname))
		}
	}),
	limits: { fileSize: 2000000 }, // In bytes: 2000000 bytes = 2 MB
	fileFilter: function (req, file, cb) {
		checkFileType(file, cb);
	}
}).single('featureImg');
/**
 * Single thumb Img Upload
 */
const thumbImgUpload = multer({
	storage: multerS3({
		s3: s3,
		bucket: 'file-uploads-application-management',
		acl: 'public-read',
		key: function (req, file, cb) {
			cb(null, path.basename(file.originalname, path.extname(file.originalname)) + '-' + Date.now() + path.extname(file.originalname))
		}
	}),
	limits: { fileSize: 2000000 }, // In bytes: 2000000 bytes = 2 MB
	fileFilter: function (req, file, cb) {
		checkFileType(file, cb);
	}
}).single('thumbImg');

// Multiple File Uploads 
const screenshotsUpload = multer({
	storage: multerS3({
		s3: s3,
		bucket: 'file-uploads-application-management',
		acl: 'public-read',
		key: function (req, file, cb) {
			cb(null, path.basename(file.originalname, path.extname(file.originalname)) + '-' + Date.now() + path.extname(file.originalname))
		}
	}),
	limits: { fileSize: 2000000 }, // In bytes: 2000000 bytes = 2 MB
	fileFilter: function (req, file, cb) {
		checkFileType(file, cb);
	}
}).array('screenshots', 50);

/**
 * Single business logo Upload
 */
const businessLogoUpload = multer({
	storage: multerS3({
		s3: s3,
		bucket: 'file-uploads-application-management',
		acl: 'public-read',
		key: function (req, file, cb) {
			cb(null, path.basename(file.originalname, path.extname(file.originalname)) + '-' + Date.now() + path.extname(file.originalname))
		}
	}),
	limits: { fileSize: 2000000 }, // In bytes: 2000000 bytes = 2 MB
	fileFilter: function (req, file, cb) {
		checkFileType(file, cb);
	}
}).single('business_logo');

router.post('/upload/featureImg', (req, res) => {
	featureImgUpload(req, res, (error) => {
		// console.log('requestOkokok', req.file);
		// console.log('error', error);
		if (error) {
			console.log('errors', error);
			res.json({ error: error });
		} else {
			// If File not found
			if (req.file === undefined) {
				console.log('Error: No File Selected!');
				res.json('Error: No File Selected');
			} else {
				// If Success
				const imageName = req.file.key;
				const imageLocation = req.file.location;
				// Save the file name into database into profile model
				res.json({
					image: imageName,
					location: imageLocation
				});
			}
		}
	});
});

router.post('/upload/businses_logo', (req, res) => {
	businessLogoUpload(req, res, (error) => {
		// console.log('requestOkokok', req.file);
		// console.log('error', error);
		if (error) {
			console.log('errors', error);
			res.json({ error: error });
		} else {
			// If File not found
			if (req.file === undefined) {
				console.log('Error: No File Selected!');
				res.json('Error: No File Selected');
			} else {
				// If Success
				const imageName = req.file.key;
				const imageLocation = req.file.location;
				// Save the file name into database into profile model
				res.json({
					image: imageName,
					location: imageLocation
				});
			}
		}
	});
});

router.post('/upload/thumbImg', (req, res) => {
	thumbImgUpload(req, res, (error) => {
		// console.log('requestOkokok', req.file);
		// console.log('error', error);
		if (error) {
			console.log('errors', error);
			res.json({ error: error });
		} else {
			// If File not found
			if (req.file === undefined) {
				console.log('Error: No File Selected!');
				res.json('Error: No File Selected');
			} else {
				// If Success
				const imageName = req.file.key;
				const imageLocation = req.file.location;
				// Save the file name into database into profile model
				res.json({
					image: imageName,
					location: imageLocation
				});
			}
		}
	});
});

router.post('/upload/screenshots', (req, res) => {
	screenshotsUpload(req, res, (error) => {
		// console.log('files', req.files);
		if (error) {
			console.log('errors', error);
			res.json({ error: error });
		} else {
			// If File not found
			if (req.files === undefined) {
				console.log('Error: No File Selected!');
				res.json('Error: No File Selected');
			} else {
				// If Success
				let fileArray = req.files,
					fileLocation;
				const galleryImgLocationArray = [];
				for (let i = 0; i < fileArray.length; i++) {
					fileLocation = fileArray[i].location;
					console.log('filenm', fileLocation);
					galleryImgLocationArray.push(fileLocation)
				}
				console.log(galleryImgLocationArray)
				// Save the file name into database
				res.json({
					filesArray: fileArray,
					locationArray: galleryImgLocationArray
				});
			}
		}
	});
});

// Multiple File Uploads 
const recieptImgUpload = multer({
	storage: multerS3({
		s3: s3,
		bucket: 'file-uploads-reciepts',
		acl: 'public-read',
		key: function (req, file, cb) {
			cb(null, path.basename(file.originalname, path.extname(file.originalname)) + '-' + Date.now() + path.extname(file.originalname))
		}
	}),
	// limits: { fileSize: 2000000 }, // In bytes: 2000000 bytes = 2 MB
	// fileFilter: function (req, file, cb) {
	// 	checkFileType(file, cb);
	// }
}).array('resume', 50);

router.post('/upload/reciept', (req, res) => {
	recieptImgUpload(req, res, (error) => {
		// console.log('files', req.files);
		if (error) {
			console.log('errors', error);
			res.json({ error: error });
		} else {
			// If File not found
			if (req.files === undefined) {
				console.log('Error: No File Selected!');
				res.json('Error: No File Selected');
			} else {
				// If Success
				let fileArray = req.files,
					fileLocation;
				const galleryImgLocationArray = [];
				for (let i = 0; i < fileArray.length; i++) {
					fileLocation = fileArray[i].location;
					// console.log('filenm', fileLocation);
					galleryImgLocationArray.push(fileLocation)
				}
				console.log(galleryImgLocationArray)
				// Save the file name into database
				res.json({
					filesArray: fileArray,
					locationArray: galleryImgLocationArray
				});
			}
		}
	});
});


//  Blog uplaod start here
/**
 * Single  Blog feature Img Upload
 */
const blogFeatureImgUpload = multer({
	storage: multerS3({
		s3: s3,
		bucket: 'file-upload-blog',
		acl: 'public-read',
		key: function (req, file, cb) {
			cb(null, path.basename(file.originalname, path.extname(file.originalname)) + '-' + Date.now() + path.extname(file.originalname))
		}
	}),
	limits: { fileSize: 2000000 }, // In bytes: 2000000 bytes = 2 MB
	fileFilter: function (req, file, cb) {
		checkFileType(file, cb);
	}
}).single('BlogFeatureImg');


router.post('/upload/blogFeatured', (req, res) => {
	blogFeatureImgUpload(req, res, (error) => {
		// console.log('requestOkokok', req.file);
		// console.log('error', error);
		if (error) {
			console.log('errors', error);
			res.json({ error: error });
		} else {
			// If File not found
			if (req.file === undefined) {
				console.log('Error: No File Selected!');
				res.json('Error: No File Selected');
			} else {
				// If Success
				const imageName = req.file.key;
				const imageLocation = req.file.location;
				// Save the file name into database into profile model
				res.json({
					image: imageName,
					location: imageLocation
				});
			}
		}
	});
});

// Blog Upload end here

module.exports = router;