const mongoose = require('mongoose')
const validator = require('validator')
const moment = require('moment')

const Schema = mongoose.Schema


const contactSchema = new Schema({
	UID: {
		type: String,
		required: true
	},
	name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		lowercase: true,
		validate(value) {
			if (!validator.isEmail(value)) {
				throw new Error('please type in a valid email')
			}
		}
	},
	category: {
		type: String,
	},
	description: {
		type: String,
	},
	createdAt: {
		type: String,
		default: moment().format(),
	},
	submittedAt: {
		type: String,
		default: Date.now(),
	},
	ticket_no: {
		type: Number,
		required: true,
		default: Date.now()

	},
	mobile: {
		type: String,
	},
	country: {
		type: String,
	},
	interested_in: {
		type: String
	},
	type: {
		type: String,
		default: "query"
	},
	subject: {
		type: String
	},
	app_name: {
		type: String
	},
	admin_url: {
		type: String
	},
	business_name: {
		type: String
	},
	languages: [{
		type: String
	}],
	budget: {
		type: String
	}
})

const Contact = mongoose.model("Contact", contactSchema)
module.exports = Contact