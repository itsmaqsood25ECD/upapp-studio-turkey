const mongoose = require('mongoose')

const Schema = mongoose.Schema

const CareersSchema = new Schema({
  en: {
    job_title: {
      type: String,
      required: true
    },
    job_vacancy: {
      type: String,
      required: true
    },
    job_exp: {
      type: String,
      required: true
    },
    job_location: {
      type: String,
      required: true
    },
  },
  ar: {
    job_titleAR: {
      type: String,
      required: true
    },

    job_vacancyAR: {
      type: String,
      required: true
    },

    job_expAR: {
      type: String,
      required: true
    },

    job_locationAR: {
      type: String,
      required: true
    },
  },
  created_at: {
    type: String,
    required: true,
    default: Date.now
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  UID: {
    type: String
  }
})

const Careers = mongoose.model("Careers", CareersSchema)
module.exports = Careers