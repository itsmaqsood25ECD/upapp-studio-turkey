/* eslint react/prop-types: 0 */
import React, { Fragment, useState, useEffect } from "react";
import { HashLink as NavLink } from 'react-router-hash-link';
import { connect } from "react-redux";
import { Layout, Menu, Select, Form, Drawer, Button, Icon, Switch } from "antd";
import "../../assets/css/homeNew.css";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import axios from 'axios'
import 'animate.css'
// let filteredApps;


const {REACT_APP_IPSTACK_KEY} = process.env;

const { Header } = Layout;
const { Option } = Select;

const HeaderComp = ({ direction }) => {

  
  const { t } = useTranslation();

  const [headerLink, setHeaderLink] = useState({
    visible: false,
  });

  const [state, setState] = useState({
    emVisible: true,
    menuVisible: true,
    language: "EN"
  });


  const { emVisible, menuVisible, language } = state;

  useEffect(() => {

 

  fetchCountryData();
    if(localStorage.getItem('lng') === 'TR'){
    i18next.changeLanguage('TR')
    }else {
    i18next.changeLanguage('TR')
    }

}, []);


const fetchCountryData = async () => {

  try {
    console.log("This statement works")

    const response = await axios.get(
      `https://api.ipstack.com/check?access_key=32e8b62adfadea003abc9299f80931ea&format=1`
    );

    console.log("fetch response",response);

    if(response.status === 200){
      
      if(response.data.country_code == 'TR'){
        sessionStorage.setItem("current_country", response.data.currency.code)
        if(!localStorage.getItem('lng')){
          localStorage.setItem('lng','TR');
          i18next.changeLanguage('TR')
        }
      }else{
        if(!localStorage.getItem('lng')){
        sessionStorage.setItem("current_country", response.data.currency.code)
        localStorage.setItem('lng','EN');
        i18next.changeLanguage('EN')
        }
      }
    
    }

   
  }
  catch (error) {
    console.log("Error has been handled")
   }

};


  const toggleMenu = () => {

    setState({
      ...state,
      menuVisible: !menuVisible
    })
  }
  const { visible } = headerLink;

  const onClose = () => {
    setHeaderLink({
      visible: false
    });
  };

  const showDrawer = () => {
    setHeaderLink({
      visible: true
    });
  };

  const LanguageChanged = (lang) => {
    console.log(lang);
    localStorage.setItem('lng', lang)

    setState({
      ...state,
      Language: localStorage.getItem('lng')
    })
    window.location.reload()
  }


  const generalHeader = (
    <Fragment>
      <Header className="responsive-header-menu">
        <NavLink className="brand-logo" to="/">
          <div className="logo" />
        </NavLink>
        <Button type="" style={{ marginLeft: "50px", fontSize: "30px", border: "2px solid" }} onClick={showDrawer} >
          <Icon type="align-right" />
        </Button>
        <Drawer title="Menu" placement={direction === DIRECTIONS.LTR ? 'left' : 'right'}
          closable={false} onClose={onClose} visible={visible}>
          <Menu className="drawer-v-Menu" theme="light" mode="vertical" defaultSelectedKeys={["0"]} onSelect={onClose}>
            <Menu.Item key="dr-factory">
              {/* <NavLink to="/factory" className="mobile-menu-name-data">About Us</NavLink> */}
              <a activeClassName="is-active" href="https://upappfactory.com/about-us" className="mobile-menu-name-data" target="_blank">
                <Icon type="info-circle" />
                {t("header_Aboutus")}
                </a>
            </Menu.Item>
            <Menu.Item key="dr-pricing">
              <NavLink to="#pageClinentale" className="mobile-menu-name-data">
              <Icon type="team" />
              {t("header_Clientele")} </NavLink>
            </Menu.Item>
            <Menu.Item key="dr-support">
              <NavLink to="#pageBlog" className="mobile-menu-name-data">
                <Icon type="contacts" />
                {t("header_Blog")}</NavLink>
            </Menu.Item>
            <Menu.Item key="dr-support">
              <NavLink to="#pageContact" className="mobile-menu-name-data">
                <Icon type="customer-service"/>
                {t("header_ContactUs")}</NavLink>
            </Menu.Item>
          </Menu>
        </Drawer>
      </Header>
      {direction === DIRECTIONS.LTR && <Fragment>
        <Header className="header-comp animate__animated ">
          <NavLink to="/">
            <div className="logo" />
          </NavLink>

          <div className="menuToggleButton" onClick={toggleMenu}>
            {console.log("menuVisible", menuVisible)}
            <svg id="Group_2" data-name="Group 2" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
              <g id="Rectangle_57" data-name="Rectangle 57" transform="translate(19 19)" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
                <rect width="13" height="13" rx="2" stroke="none" />
                <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none" />
              </g>
              <g id="Rectangle_60" data-name="Rectangle 60" transform="translate(19)" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
                <rect width="13" height="13" rx="2" stroke="none" />
                <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none" />
              </g>
              <g id="Rectangle_58" data-name="Rectangle 58" transform="translate(0 19)" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
                <rect width="13" height="13" rx="2" stroke="none" />
                <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none" />
              </g>
              <g id="Rectangle_59" data-name="Rectangle 59" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
                <rect width="13" height="13" rx="2" stroke="none" />
                <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none" />
              </g>
            </svg>
          </div>
          {menuVisible && <Menu className="U-Menu animate__animated animate__fadeInRight" theme="light" mode="horizontal" defaultSelectedKeys={["0"]}>
           <Menu.Item >
              <NavLink to="#pageClinentale">Clientele</NavLink>
            </Menu.Item>
           <Menu.Item key="n-factory">
              {/* <NavLink activeClassName="is-active" to="https://upappfactory.com/about-us" target="_blank">About Us</NavLink> */}
              <a activeClassName="is-active" href="https://upappfactory.com/about-us" target="_blank">{t("header_Aboutus")}</a>
            </Menu.Item>
            <Menu.Item >
              <NavLink to="#pageClinentale">{t("header_Clientele")}</NavLink>
            </Menu.Item>
            {/* <Menu.Item >
              <NavLink to="#uaf_cp_id_consultant">Management</NavLink>
            </Menu.Item> */}
            <Menu.Item >
              <NavLink to="#pageBlog">{t("header_Blog")}</NavLink>
            </Menu.Item>
            <Menu.Item >
              <NavLink to="#pageContact">{t("header_ContactUs")}</NavLink>
            </Menu.Item>
          </Menu>
          }

        </Header>
      </Fragment>}
      {direction === DIRECTIONS.RTL && <Fragment>
        <Header className="header-comp float-right">
          <NavLink to="/">
            <div className="logo float-right" />
          </NavLink>
          <Menu className="U-Menu" theme="light" mode="horizontal" defaultSelectedKeys={["0"]}>
            <Menu.Item >
              <a activeClassName="is-active" href="https://upappfactory.com/about-us" target="_blank">{t("header_Aboutus")}</a>
            </Menu.Item>
            <Menu.Item>
              <NavLink activeClassName="is-active" to="#pageClinentale">{t("header_Clientele")}</NavLink>
            </Menu.Item>
            {/* <Menu.Item>
              <NavLink activeClassName="is-active" to="#uaf_cp_id_consultant">Management</NavLink>
            </Menu.Item> */}
            <Menu.Item>
              <NavLink activeClassName="is-active" to="#pageBlog">{t("header_Blog")}</NavLink>
            </Menu.Item>
            <Menu.Item key="n-factory">
              <NavLink activeClassName="is-active" to="#pageContact">{t("header_ContactUs")}</NavLink>
            </Menu.Item>
          </Menu>
        </Header>
      </Fragment>}
    </Fragment>
  );

  return (
    <Fragment>
      {generalHeader}
    </Fragment>
  );
};

// const HeaderComp = Form.create({ name: "header_comp" })(HeaderForm);
HeaderComp.prototypes = {
  direction: withDirectionPropTypes.direction,
};

export default connect(null, null)(withDirection(HeaderComp));
