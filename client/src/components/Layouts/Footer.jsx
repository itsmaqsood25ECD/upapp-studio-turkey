import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Layout, Row, Col, Typography } from "antd";
import { InstagramOutlined, FacebookOutlined, LinkedinOutlined } from "@ant-design/icons";
import withDirection from 'react-with-direction';
import Logo from "../../assets/img/logowhite.png"
import sflag from "../../assets/img/singaporeflag.png"
import tflag from "../../assets/img/Flag_of_Turkey.svg"

import { useTranslation } from 'react-i18next';


const { Title, Text } = Typography;
const { Footer } = Layout;

const FooterComp = () => {

  const { t } = useTranslation();

  const generalFooter = (
    <Fragment>
      <Footer className="app-footer" style={{ paddingTop: "100px" }}>
        <Row gutter={16} type="flex" justify="center" align="top" style={{ margin: "0" }}>
          <Col lg={6} md={12} sm={24} xs={24} style={{ textAlign: "left" }}>
            <Link to="/">
              <img
                src={Logo}
                alt="its a footer logo"
                style={{ width: "50px", marginBottom: "10px" }}
              />
            </Link>
            <div>
              <p style={{ color: "#FFFFFF", maxWidth: "285px" }}>{t("Footer_desc")}</p>
            </div>
            <div>
              <Title className="fontSize-20">{t("Footer_followus")}</Title>
            </div>
            <div className="socialIcons">
              <a target="_blank" href="https://www.instagram.com/upappfactory/"><InstagramOutlined /></a>
              <a target="_blank" href="https://www.facebook.com/UpAppFactory/"><FacebookOutlined /></a>
              <a target="_blank" href="https://www.linkedin.com/company/upappfactory/"><LinkedinOutlined /></a>
            </div>
          </Col>
          <Col lg={5} md={12} sm={24} xs={24}>
            <div className="footer-section">
              <div>
                <Title className="fontSize-20">{t("Footer_location")}</Title>
              </div>
              <div>
                <Title className="fontSize-20 location"><img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" /> {t("Footer_location_city1")}</Title>
                <p style={{fontSize:"12px"}}>{t("Footer_location_city1_add")}</p>
              </div>
              <div>
                <Title className="fontSize-20 location"><img src={sflag} /> {t("Footer_location_city2")}</Title>
                <p style={{fontSize:"12px"}}>{t("Footer_location_city2_add")}</p>
              </div>
              {/* <div>
                <Title className="fontSize-20 location"><img src={tflag} /> {t("Footer_location_city3")}</Title>
                <p style={{fontSize:"12px"}}>{t("Footer_location_city3_add")}</p>
              </div> */}
            </div>
          </Col>
          <Col lg={4} md={12} sm={24} xs={12}>
            <div className="footer-section">
              <div>
                <Title className="fontSize-20">{t("Footer_ourproduct")}</Title>
              </div>
              <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/eCommerce-Single-Vendor-Platform">
                  <Text>{t("Footer_ourproduct_ecommerce")}</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/facility-management-application">
                  <Text>{t("Footer_ourproduct_facility")}</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/restaurant-application">
                  <Text>{t("Footer_ourproduct_restaurant")}</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/upappbot">
                  <Text>{t("Footer_ourproduct_upappbot")}</Text>
                </a>
              </div>
            </div>
          </Col>
          <Col lg={4} md={12} sm={24} xs={12}>
            <div className="footer-section">
              <div>
                <Title className="fontSize-20">{t("Footer_ourcompany")}</Title>
              </div>
              <div className="footer-nav-item">
                {/* <Link to="https://upappfactory.com/about-us">
                  <Text>About Us</Text>
                </Link> */}
                <a target="_blank" href="https://upappfactory.com/about-us">
                  <Text>{t("Footer_ourcompany_aboutus")}</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/terms-service">
                  <Text>{t("Footer_ourcompany_TermsofService")}</Text>
                </a>
              </div>

              <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/privacy-policy">
                  <Text>{t("Footer_ourcompany_PrivacyPolicy")}</Text>
                </a>
              </div>

              <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/cancellation-and-refund-policy">
                  <Text>{t("Footer_ourcompany_Cancellation&RefundPolicy")}</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/contactUs">
                  <Text>{t("Footer_ourcompany_ContactUs")}</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/clientele">
                  <Text>{t("Footer_ourcompany_Clientele")}</Text>
                </a>
              </div>
              <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/blog">
                  <Text>{t("Footer_ourcompany_Blogs")}</Text>
                </a>
              </div>
            </div>
          </Col>

        </Row>
        <Row gutter={16} style={{ margin: 0, padding: "30px 0px 10px 0px" }}>
          <Col lg={24} md={24} sm={24} xs={24} style={{ padding: "5px", textAlign: "center" }} className="footer-copy-right">
            <Link to="/">UPapp factory</Link> ©2021 all rights are reserved
          </Col>
        </Row>
      </Footer>
    </Fragment>
  );
  return (
    <Fragment>
      {generalFooter}
    </Fragment>
  );
};


export default connect(null, null)(
  withDirection(FooterComp)
);
