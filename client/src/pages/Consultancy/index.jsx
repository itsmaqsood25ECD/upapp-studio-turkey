import React, { Fragment, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Icon, Card, Button, Typography, Row, Col, List, Spin, Input, Form, Anchor } from "antd";
import "antd/dist/antd.css";
import "../../assets/css/custom.css";
import "../../assets/css/main.css";
import "../../assets/css/home.css";
import "../../assets/css/homeNew.css";
import moment from 'moment';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import { useTranslation } from 'react-i18next';


import { getRecentBlog } from "../../actions/blog.management.action";

// img
import heroImg from "../../assets/img/hero.png"
import img1 from "../../assets/img/img1.png"
import img2 from "../../assets/img/img2.png"
import img3 from "../../assets/img/img3.png"
import img4 from "../../assets/img/img4.png"
import img6 from "../../assets/img/img6.png"
import icon1 from "../../assets/img/plan.svg"
import icon2 from "../../assets/img/pen.svg"
import icon3 from "../../assets/img/code.svg"
import icon4 from "../../assets/img/surface.svg"
import icon5 from "../../assets/img/project.svg"
import icon6 from "../../assets/img/user.svg"
import discussion from "../../assets/img/businessmen.svg"
import strategy from "../../assets/img/strategy.svg"
import design from "../../assets/img/ux.svg"
import coding from "../../assets/img/coding.svg"
import testing from "../../assets/img/approval.svg"
import launch from "../../assets/img/speed.svg"
import darnijood from "../../assets/img/darnijooddard.png"
import cs1 from "../../assets/img/cs1.png"
import cs2 from "../../assets/img/cs2.png"
import cs3 from "../../assets/img/cs3.png"
import cs4 from "../../assets/img/cs4.png"
import ConsultancyContactUs from './contactUs'

const { Meta } = Card;
const Consultancy = ({getRecentBlog, recentBlog}) => {

  const { t } = useTranslation();

    useEffect(() => {
      getRecentBlog()
      console.log(recentBlog);

    }, [])
    
    console.log(recentBlog);

  function NextArrow(props) {
    const { onClick } = props;
    return (
      <Button
        type="primary"
        onClick={onClick}
        className="ss-next-btn"
        shape="circle"
      >
        <Icon type="arrow-right" />
      </Button>
    );
  }

  function PrevArrow(props) {
    const { onClick } = props;
    return (
      <Button
        type="primary"
        onClick={onClick}
        className="ss-prev-btn"
        shape="circle"
      >
        <Icon type="arrow-left" />
      </Button>
    );
  }

  var settings = {
    // prevArrow: <PrevArrow />,
    prevArrow: false,
    // nextArrow: <NextArrow />,
    nextArrow: false,
    dots: false,
    // infinite: true,
    centerMode: true,
    speed: 200,
    swipeToSlide: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    lazyLoad: true,
    initialSlide: 0,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          // infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1
        }
      }
    ]
  };
  return (
    <React.Fragment>
      <div className="uaf_cp">
        {/* ===================
                    Start Landing  
                ===================  */}
        <div className="uaf_std_hero">
          <div className="UAF-container-fluid">
            <Row type="flex" justify="space-around" align="middle">
              <Col xs={{ span: 24, order: 2 }} md={{ span: 18, order: 2 }} lg={{ span: 12, order: 1 }} className="alignSelfCenter">
                <h1 className="uaf_hero_title1">{t("Home_Studio")}</h1>
                <hr className="uaf_hero_line1" />
                <div className="uaf_hero_content">
                  <h3 className="uaf_hero_content_title">{t("Home_subheading1")}<br />{t("Home_subheading2")}</h3>
                  <p className="uaf_hero_content_text">{t("Home_desc")}</p>
                </div>
              </Col>
              <Col xs={{ span: 24, order: 1 }} md={{ span: 18, order: 1 }} lg={{ span: 12, order: 2 }}>
                <img src={heroImg} className="heroImage" />
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Landing  
                ===================  */}

        {/* ===================
                    Start Section1  
                ===================  */}
        <div className="uaf_std_section1" id="ideation">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="uaf_std_section_cntnt">
                <Row type="flex" justify="space-around">
                  <Col xs={{ span: "24", order: 2 }} md={{ span: "15", order: 2 }} lg={{ span: 11, order: 1 }} className="uaf_std_section_left">
                    <h3>{t("Home_section_1_heading")}</h3>
                    <p>{t("Home_section_1_desc")}</p>
                    <a href="#pageContact">{t("Get_Started")}</a>
                  </Col>
                  <Col xs={{ span: "24", order: 1 }} md={{ span: "15", order: 1 }} lg={{ span: 13, order: 2 }} className="uaf_std_section_right">
                    <img src={img2} />
                  </Col>
                </Row>
              </Col>
              <Col xs={{ span: 24, order: 3 }} md={{ span: 24, order: 3 }} lg={{ span: 24, order: 2 }} className="uaf_std_section_header">
                <h3>
                {t("Home_section_2_heading1")}<br /> {t("Home_section_2_heading2")}
                </h3>
                <hr className="uaf_std_section_header_line" />
                <p className="uaf_std_section_header_text">{t("Home_section_2_desc")}</p>
              </Col>
              <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 16, order: 3 }} className="uaf_std_section_imgmain">
                <img src={img1} />
              </Col>
            </Row>
          </div>
          <div className="idealProcess">
            <div className="UAF-container-fluid">
              <Row type="flex" align="middle" className="idealProcessMain">
                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} sm={{ span: 24, order: 1 }} className="idealProcess_heading">
                  <h3>{t("Home_section_3_heading1")}<br /> {t("Home_section_3_heading2")}</h3>
                  <hr />
                </Col>
                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} sm={{ span: 24, order: 1 }} className="idealProcess_mainContent">
                  <Row type="flex" align="middle" justify="center">
                    <Col xs={{ span: 24, order: 1 }} md={{ span: 8, order: 1 }} lg={{ span: 4, order: 1 }} className="idealProcess_mainContent_col">
                      <div className="idealProcess_mainContent_col_innermain">
                        <div>
                          <img src={icon1} />
                        </div>
                        <p>{t("Home_section_3_type_1")}</p>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 2 }} md={{ span: 8, order: 2 }} lg={{ span: 4, order: 2 }} className="idealProcess_mainContent_col">
                      <div className="idealProcess_mainContent_col_innermain">
                        <div>
                          <img src={icon2} />
                        </div>
                        <p>{t("Home_section_3_type_2")}</p>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 3 }} md={{ span: 8, order: 3 }} lg={{ span: 4, order: 3 }} className="idealProcess_mainContent_col">
                      <div className="idealProcess_mainContent_col_innermain">
                        <div>
                          <img src={icon3} />
                        </div>
                        <p>{t("Home_section_3_type_3")}</p>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 4 }} md={{ span: 8, order: 4 }} lg={{ span: 4, order: 4 }} className="idealProcess_mainContent_col">
                      <div className="idealProcess_mainContent_col_innermain">
                        <div>
                          <img src={icon4} />
                        </div>
                        <p>{t("Home_section_3_type_4")}</p>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 5 }} md={{ span: 8, order: 5 }} lg={{ span: 4, order: 5 }} className="idealProcess_mainContent_col">
                      <div className="idealProcess_mainContent_col_innermain">
                        <div>
                          <img src={icon5} />
                        </div>
                        <p>{t("Home_section_3_type_5")}</p>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 6 }} md={{ span: 8, order: 6 }} lg={{ span: 4, order: 6 }} className="idealProcess_mainContent_col">
                      <div className="idealProcess_mainContent_col_innermain">
                        <div>
                          <img src={icon6} />
                        </div>
                        <p>{t("Home_section_3_type_6")}</p>
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
          </div>
        </div>
        {/* ===================
                    End Section1  
                ===================  */}
        {/* ===================
                    Start Section2  
                ===================  */}
        <div className="uaf_std_section2" id="brainstorm">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} className="uaf_std_section_header">
                <h3>
                {t("Home_section_4_mainheading")}
                </h3>
                <hr className="uaf_std_section_header_line" />
              </Col>
              <Col lg={{ span: 24, order: 1 }}>
                <Row type="flex" align="middle" className="uaf_std_section2_imgRows">
                  <Col xs={{ span: "24", order: 2 }} md={{ span: "15", order: 2 }} lg={{ span: 11, order: 1 }} className="uaf_std_section_left">
                    <h3>{t("Home_section_4_heading1")}<br />{t("Home_section_4_heading2")}</h3>
                    <p>{t("Home_section_4_desc")}</p>
                    <a href="#pageContact">{t("Get_Started")}</a>
                  </Col>
                  <Col xs={{ span: "24", order: 1 }} md={{ span: "15", order: 1 }} lg={{ span: 13, order: 2 }} className="uaf_std_section_right">
                    <img src={img4} />
                  </Col>
                </Row>
                <Row type="flex" align="middle" className="uaf_std_section2_imgRows">
                  <Col xs={{ span: "24", order: 2 }} md={{ span: "15", order: 2 }} lg={{ span: 11, order: 2 }} className="uaf_std_section_right">
                    <h3>{t("Home_section_5_heading")}</h3>
                    <p>{t("Home_section_5_desc")}</p>
                    <a href="#pageContact">{t("Get_Started")}</a>
                  </Col>
                  <Col xs={{ span: "24", order: 1 }} md={{ span: "15", order: 1 }} lg={{ span: 13, order: 1 }} className="uaf_std_section_left">
                    <img src={img3} />
                  </Col>
                </Row>
                <Row type="flex" align="middle" className="uaf_std_section2_imgRows">
                  <Col xs={{ span: "24", order: 2 }} md={{ span: "15", order: 2 }} lg={{ span: 11, order: 1 }} className="uaf_std_section_left">
                    <h3>{t("Home_section_6_heading")}</h3>
                    <p>{t("Home_section_6_desc")}</p>
                    <a href="#pageContact">{t("Get_Started")}</a>
                  </Col>
                  <Col xs={{ span: "24", order: 1 }} md={{ span: "15", order: 1 }} lg={{ span: 13, order: 2 }} className="uaf_std_section_right">
                    <img src={img6} />
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div className="wonderingHow">
            <div className="UAF-container-fluid">
              <Row type="flex" align="middle" className="wonderingHowMain">
                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} sm={{ span: 24, order: 1 }} className="wonderingHow_heading">
                  <h3>{t("Home_section_7_heading")}</h3>
                  <hr />
                  <p className="uaf_std_section_header_text_dark">{t("Home_section_7_desc")}</p>
                </Col>
                <Col lg={{ span: 24, order: 2 }} md={{ span: 20, order: 2 }} sm={{ span: 24, order: 2 }} className="wonderingHow_mainContent">
                  <Row type="flex" justify="start">
                    <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 8, order: 1 }} className="wonderingHow_mainContent_col">
                      <div className="wonderingHow_mainContent_col_innermain">
                        <img src={discussion} />
                        <h4>{t("Home_section_7_feature_1_heading")}</h4>
                        <p>{t("Home_section_7_feature_1_desc")}</p>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 8, order: 2 }} className="wonderingHow_mainContent_col">
                      <div className="wonderingHow_mainContent_col_innermain">
                        <img src={strategy} />
                        <h4>{t("Home_section_7_feature_2_heading")}</h4>
                        <p>{t("Home_section_7_feature_2_desc")}</p>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 3 }} md={{ span: 12, order: 3 }} lg={{ span: 8, order: 3 }} className="wonderingHow_mainContent_col">
                      <div className="wonderingHow_mainContent_col_innermain">
                        <img src={design} />
                        <h4>{t("Home_section_7_feature_3_heading")}</h4>
                        <p>{t("Home_section_7_feature_3_desc")}</p>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 4 }} md={{ span: 12, order: 4 }} lg={{ span: 8, order: 4 }} className="wonderingHow_mainContent_col">
                      <div className="wonderingHow_mainContent_col_innermain">
                        <img src={coding} />
                        <h4>{t("Home_section_7_feature_4_heading")}</h4>
                        <p>{t("Home_section_7_feature_4_desc")}</p>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 5 }} md={{ span: 12, order: 5 }} lg={{ span: 8, order: 5 }} className="wonderingHow_mainContent_col">
                      <div className="wonderingHow_mainContent_col_innermain">
                        <img src={testing} />
                        <h4>{t("Home_section_7_feature_5_heading")}</h4>
                        <p>{t("Home_section_7_feature_5_desc")}</p>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 6 }} md={{ span: 12, order: 6 }} lg={{ span: 8, order: 6 }} className="wonderingHow_mainContent_col">
                      <div className="wonderingHow_mainContent_col_innermain">
                        <img src={launch} />
                        <h4>{t("Home_section_7_feature_6_heading")}</h4>
                        <p>{t("Home_section_7_feature_6_desc")}</p>
                      </div>
                    </Col>
                  </Row>
                </Col>
                <Col lg={{ span: 24, order: 3 }} md={{ span: 20, order: 3 }} sm={{ span: 24, order: 3 }} className="wonderingHowMainBtn">
                  <a href="#pageContact">{t("Get_Started")}</a>
                </Col>
              </Row>
            </div>
          </div>
        </div>
        {/* ===================
                    End Section2  
                ===================  */}
        {/* ===================
                    Start Section3  
                ===================  */}
        <div className="uaf_std_section3" id="productDesign">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} className="uaf_std_section_header">
                <h3>
                {t("Home_section_8_heading")}
                </h3>
                <hr className="uaf_std_section_header_line" />
                <p className="uaf_std_section_header_text">{t("Home_section_8_desc1")}<br />{t("Home_section_8_desc2")}</p>
              </Col>
              <Col lg={{ span: 24, order: 2 }} md={{ span: 20, order: 2 }} sm={{ span: 24, order: 2 }} className="digitalPresence_mainContent">
                <Row type="flex" justify="start" className="digitalPresence_mainContentRow1">
                  <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 6, order: 1 }} className="digitalPresence_mainContent_col">
                    <div className="digitalPresence_mainContent_col_innermain">
                      <h4>{t("Home_section_8_feature_1_heading")}</h4>
                      <p>{t("Home_section_8_feature_1_desc")}</p>
                    </div>
                  </Col>
                  <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 6, order: 2 }} className="digitalPresence_mainContent_col">
                    <div className="digitalPresence_mainContent_col_innermain">
                      <h4>{t("Home_section_8_feature_2_heading")}</h4>
                      <p>{t("Home_section_8_feature_2_desc")}</p>
                    </div>
                  </Col>
                  <Col xs={{ span: 24, order: 3 }} md={{ span: 12, order: 3 }} lg={{ span: 6, order: 3 }} className="digitalPresence_mainContent_col">
                    <div className="digitalPresence_mainContent_col_innermain">
                      <h4>{t("Home_section_8_feature_3_heading")}</h4>
                      <p>{t("Home_section_8_feature_3_desc")}</p>
                    </div>
                  </Col>
                  <Col xs={{ span: 24, order: 4 }} md={{ span: 12, order: 4 }} lg={{ span: 6, order: 4 }} className="digitalPresence_mainContent_col">
                    <div className="digitalPresence_mainContent_col_innermain">
                      <h4>{t("Home_section_8_feature_4_heading")}</h4>
                      <p>{t("Home_section_8_feature_4_desc")}</p>
                    </div>
                  </Col>
                </Row>
                <Row type="flex" justify="center" className="digitalPresence_mainContentRow2">
                  <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 7, order: 1 }} className="digitalPresence_mainContent_col">
                    <div className="digitalPresence_mainContent_col_innermain">
                      <h4>{t("Home_section_8_feature_5_heading")}</h4>
                      <p>{t("Home_section_8_feature_5_desc")}</p>
                    </div>
                  </Col>
                  <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 7, order: 2 }} className="digitalPresence_mainContent_col">
                    <div className="digitalPresence_mainContent_col_innermain">
                      <h4>{t("Home_section_8_feature_6_heading")}</h4>
                      <p>{t("Home_section_8_feature_6_desc")}</p>
                    </div>
                  </Col>
                  <Col xs={{ span: 24, order: 3 }} md={{ span: 12, order: 3 }} lg={{ span: 7, order: 3 }} className="digitalPresence_mainContent_col">
                    <div className="digitalPresence_mainContent_col_innermain">
                      <h4>{t("Home_section_8_feature_7_heading")}</h4>
                      <p>{t("Home_section_8_feature_7_desc")}</p>
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col lg={{ span: 24, order: 3 }} md={{ span: 20, order: 3 }} sm={{ span: 24, order: 3 }} className="digitalPresenceMainBtn">
                <a href="#pageContact">{t("Get_Started")}</a>
              </Col>
            </Row>
          </div>
          <div className="projectCaseStudyBody" id="pageClinentale">
            <div className="UAF-container-fluid">
              <Row type="flex" align="middle">
                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} sm={{ span: 24, order: 1 }} className="CaseStudy_heading">
                  <h3>{t("Home_section_9_heading")}</h3>
                  <hr />
                </Col>
                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} sm={{ span: 24, order: 1 }} className="CaseStudy_contentMain">
                  <Row type="flex" align="middle">
                    <Col lg={{ span: 12, order: 1 }} md={{ span: 20, order: 1 }} sm={{ span: 24, order: 1 }} className="CaseStudy_cols">
                      <img src={cs1} className="caseStudyImg" />
                      <div className="casestudyLogo">
                        <img className="companyLogo nobgimg" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/SalamPropertiesLogo.png" />
                        <h4 className="companyLogoName">{t("Home_section_9_Clientele_1_heading")}</h4>
                      </div>
                      <div className="casestudyhoverBlocks">
                        <p>{t("Home_section_9_Clientele_1_desc")}</p>
                      </div>
                    </Col>
                    <Col lg={{ span: 12, order: 2 }} md={{ span: 20, order: 2 }} sm={{ span: 24, order: 2 }} className="CaseStudy_cols">
                      <img src={cs2} className="caseStudyImg" />
                      <div className="casestudyLogo">
                        <img className="companyLogo" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Fursahh_Logo_Final.png" />
                        <h4 className="companyLogoName">{t("Home_section_9_Clientele_2_heading")}</h4>
                      </div>
                      <div className="casestudyhoverBlocks">
                        <p>{t("Home_section_9_Clientele_2_desc")}</p>
                      </div>
                    </Col>
                    <Col lg={{ span: 12, order: 3 }} md={{ span: 20, order: 3 }} sm={{ span: 24, order: 3 }} className="CaseStudy_cols">
                      <img src={cs3} className="caseStudyImg" />
                      <div className="casestudyLogo">
                        <img className="companyLogo nobgimg" src={darnijood} />
                        <h4 className="companyLogoName">{t("Home_section_9_Clientele_3_heading")}</h4>
                      </div>
                      <div className="casestudyhoverBlocks">
                        <p>{t("Home_section_9_Clientele_3_desc")}</p>
                      </div>
                    </Col>
                    <Col lg={{ span: 12, order: 4 }} md={{ span: 20, order: 4 }} sm={{ span: 24, order: 4 }} className="CaseStudy_cols">
                      <img src={cs4} className="caseStudyImg" />
                      <div className="casestudyLogo">
                        <img className="companyLogo" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Comex+3%402x.png" />
                        <h4 className="companyLogoName">{t("Home_section_9_Clientele_4_heading")}</h4>
                      </div>
                      <div className="casestudyhoverBlocks">
                        <p>{t("Home_section_9_Clientele_4_desc")}</p>
                      </div>
                    </Col>
                  </Row>
                </Col>
                <Col lg={{ span: 24, order: 3 }} md={{ span: 20, order: 3 }} sm={{ span: 24, order: 3 }} className="digitalPresenceMainBtn">
                    <a href="https://upappfactory.com/clientele" target="_blank" rel="noopener noreferrer" >{t("Checkout_Clientele")}</a>
                  </Col>
              </Row>
            </div>
          </div>
        </div>
        {/* ===================
                    End Section3  
                ===================  */}
        {/* ===================
                    Start Section9  
                ===================  */}
        <div className="uaf_std_section9" id="pageBlog">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 24, order: 1 }} xs={{span:24,order:1}} sm={{span:24,order:1}} className="uaf_std_section_header">
                <h3>
                {t("Home_section_10_heading")}
                </h3>
                <hr className="uaf_std_section_header_line" />
              </Col>
              <Col lg={{ span: 24, order: 2 }} xs={{span:24,order:2}} sm={{span:24,order:2}} className="uaf_std_section_tile_outer">
                <Row gutter={16} type="flex" justify="space-around" align="middle">
                  <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 24 }} lg={{ span: 24 }} xl={{ span: 24 }}>
                    <Row gutter={16}>
                      <Slider {...settings} style={{ padding: "20px 50px" }}>
                        {console.log(recentBlog)}
                        {recentBlog && recentBlog.map(blog =>{
                           return <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                            <a target="_blank" href={`https://upappfactory.com/blog/${blog.slug}`}>
                              <Card className="uaf_bp_card uaf_std_card"
                                cover={<img style={{ height: "230px", width: "100%", objectFit: "cover" }} alt={blog.title} src={blog.featured_image} />}
                              >
                                <Meta description={<div>
                                <p className="uaf_bp_card-desc-heading uaf_std_card_heading">{blog.title}</p>
                                  <p className="uaf_bp_card-desc uaf_std_card_date"><span className="uaf_bp_card-desc-date">{moment(blog.date_addedd).format('ll')} | by</span><a href="javascript:void(0)" > {blog.author}</a></p>
                                </div>} />
                              </Card>
                            </a>
                          </Col>
            
                        })}
                       </Slider>
                    </Row>
                  </Col>

                  <Col lg={{ span: 24, order: 3 }} md={{ span: 20, order: 3 }} sm={{ span: 24, order: 3 }} className="digitalPresenceMainBtn">
                    <a href="https://www.upappfactory.com/blog" target="_blank" rel="noopener noreferrer" >{t("View_all")}</a>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </div>
        {/* ===================
                    End Section9  
                ===================  */}
        {/* ===================
                    Start Section  
                ===================  */}
        <div className="uaf_std_section10" id="pageContact">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle">
              <Col lg={{ span: 18, order: 1 }} className="uaf_std_section_footer" style={{ margin: "0 auto" }}>
                <h3 style={{ lineHeight: "1.2" }}>
                {t("Home_section_11_heading1")}<br />{t("Home_section_11_heading2")}
                </h3>
                <hr className="uaf_std_section_footer_line" />
              </Col>
            </Row>

            <ConsultancyContactUs />
            
          </div>
        </div>
        {/* ===================
                    End Section  
                ===================  */}

        {/*  */}
      </div>
    </React.Fragment >
  );
};


Consultancy.propTypes = {

  getRecentBlog: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({

  recentBlog: state.BlogManagement.recentBlog,

});

export default connect(mapStateToProps, {
  getRecentBlog
})(Consultancy);
