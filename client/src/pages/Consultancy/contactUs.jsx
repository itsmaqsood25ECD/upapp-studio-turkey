import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import ReCAPTCHA from "react-google-recaptcha";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import SmartInput from "react-phone-number-input/smart-input";
import { Redirect } from "react-router-dom";
import { CountryDropdown } from "react-country-region-selector";
import { Row, Col, Select, Button, Form, Input, Icon, message } from "antd";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
// import { useTranslation } from 'react-i18next';
import { StudioContact } from '../../actions/contact.actions'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next';


const recaptchaRef = React.createRef();

const FooterComp = ({
  StudioContact,
  form: { getFieldDecorator, validateFields, setFieldsValue },
  direction
}) => {
  const { t } = useTranslation();
  
  const { TextArea } = Input;
  const { Option } = Select;
  const [formData, setFormData] = useState({
    isSubmitted: false,
    HumanVarification: false,
  });

  const {
    HumanVarification,
    isSubmitted
  } = formData;


  // Forms

  const selectCountry = val => {
    setFormData({
      ...formData,
      country: val
    });
  };

  const selectgettoknow = val => {
    setFormData({
      ...formData,
      hearedAt: val
    });
  };

  const onChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  const onPhoneChange = e => {
    setFormData({
      ...formData,
      mobile: e
    });
  };

  const onSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        setFormData({
          ...formData
        });
        // console.log(formData, "body")
        StudioContact(formData);
        setFormData({
          isSubmitted: true
        });
        message.success('Request Successfull')
      }
    });
  };


  if (isSubmitted) {
    return <Redirect to="/success"></Redirect>;
  }

  const generalFooter = (
    <Fragment>
      <section id="uaf_cp_id_ContactUs">
        <Row
          // gutter={16}
          style={{ marginLeft: "0px", marginRight: "0px" }}
          type="flex"
          justify="center"
          align="middle"
        >
          <Col lg={14} md={24}>
            <div>
              <Form onSubmit={e => onSubmit(e)} className="getin-touch-form uaf_cp_contactform_Main">
                <Row gutter={30} className="uaf_cp_contactform_InnerMain">
                  <Col lg={12} xs={24}>
                    <Form.Item>
                      {getFieldDecorator("firstname", {
                        rules: [
                          {
                            required: true,
                            message: `${t("Contactus_firstname_err")}`
                          }
                        ]
                      })(
                        <Input
                          className="uaf_cp_formNameInput"
                          placeholder={t("Contactus_firstname")}
                          name="firstname"
                          onChange={e => onChange(e)}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col lg={12} xs={24}>
                    <Form.Item>
                      {getFieldDecorator("lastname", {
                        rules: [
                          {
                            required: true,
                            message: `${t("Contactus_lastname_err")}`
                          }
                        ]
                      })(
                        <Input
                          className="uaf_cp_formNameInput"
                          placeholder={t("Contactus_lastname")}
                          name="lastname"
                          onChange={e => onChange(e)}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col lg={12} xs={24}>
                    <Form.Item>
                      {getFieldDecorator("email", {
                        rules: [
                          {
                            required: true,
                            message: `${t("Contactus_email_err")}`
                          }
                        ]
                      })(
                        <Input
                          className="uaf_cp_formEmailInput"
                          placeholder={t("Contactus_email")}
                          name="email"
                          type="email"
                          onChange={e => onChange(e)}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col lg={12} xs={24}>
                    <Form.Item>
                      {getFieldDecorator("country", {
                        rules: [
                          {
                            required: true,
                            message: `${t("Contactus_country_err")}`
                          }
                        ]
                      })(
                        <CountryDropdown
                          className="uaf_cp_formCountryInput"
                          blacklist={[
                            "AF",
                            "AO",
                            "DJ",
                            "GQ",
                            "ER",
                            "GA",
                            "IR",
                            "KG",
                            "LY",
                            "MD",
                            "NP",
                            "ST",
                            "SL",
                            "SD",
                            "SY",
                            "SR",
                            "TM",
                            "VE",
                            "ZW",
                            "IL"
                          ]}
                          name="country"
                          valueType="full"
                          defaultOptionLabel={t("Contactus_country")}
                          style={{ height: "32px" }}
                          onChange={val => selectCountry(val)}
                        />
                      )}
                    </Form.Item>{" "}
                  </Col>
                  <Col lg={24} xs={24}>
                    <Form.Item>
                      {getFieldDecorator("budget", {
                        rules: [
                          {
                            required: true,
                            message: `${t("Contactus_budget_err")}`
                          }
                        ]
                      })(
                        <Input
                          className="uaf_cp_formNameInput"
                          placeholder={t("Contactus_budget")}
                          name="budget"
                          type="number"
                          onChange={e => onChange(e)}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col lg={24} xs={24}>
                    <Form.Item>
                      {getFieldDecorator("description", {
                        rules: [
                          {
                            required: true,
                            message: `${t("Contactus_idea_err")}`
                          }
                        ]
                      })(
                        <TextArea
                          className="uaf_cp_formbAboutInput"
                          name="description"
                          onChange={e => onChange(e)}
                          placeholder={t("Contactus_idea")}
                          autosize={{ minRows: 4, maxRows: 6 }}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col lg={24} xs={24}>
                    <Form.Item>
                      {getFieldDecorator("hearedAt", {
                        rules: [
                          {
                            required: true,
                            message: `${t("Contactus_hearus_err")}`
                          }
                        ]
                      }
                      )(

                        <Select
                          className="uaf_cp_formCountryInput"
                          name="hearedAt"
                          onChange={val => selectgettoknow(val)}
                          placeholder={t("Contactus_hearus")}
                        >
                          <Option key="facebook" value="facebook">
                            FaceBook
                          </Option>
                          <Option key="Instagram" value="Instagram">
                            Instagram
                          </Option>
                          <Option key="Linkedin" value="Linkedin">
                            Linkedin
                          </Option>
                          <Option key="Twitter" value="Twitter">
                            Twitter
                          </Option>
                          <Option key="Friend" value="Friend">
                            Friend
                          </Option>
                          <Option key="Others" value="Others">
                            Others
                          </Option>
                        </Select>

                      )}
                    </Form.Item>{" "}
                  </Col>
                  <Col lg={24} xs={24}>
                    <Form.Item style={{ textAlign: "left" }}>
                      <Button
                        className="uaf_cp-speakwith-btn"
                        type="primary"
                        htmlType="submit"
                        style={{
                          height: "40px",
                          fontSize: "16px",
                          width: "100%",
                          textTransform: "uppercase",
                          fontWeight: "500"
                        }}
                      >
                       {t("submit")}
                        </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </div>
          </Col>
        </Row>
      </section>
    </Fragment>
  );

  return (
    <Fragment>
      {generalFooter}
    </Fragment>
  );
};

const FooterCompForm = Form.create({ name: "Get_in_Touch" })(FooterComp);

FooterCompForm.prototypes = {
  direction: withDirectionPropTypes.direction,
  StudioContact: PropTypes.func.isRequired,
};


export default connect(null, { StudioContact })(
  withDirection(FooterCompForm)
);
