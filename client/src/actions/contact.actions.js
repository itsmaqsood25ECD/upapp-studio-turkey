import axios from "axios";
import {CONTACT_SUCCESS, CONTACT_FAIL} from './constants'

// Conatct Form Submission
export const StudioContact = ({
  firstname,
  lastname,
  email,
  country,
  budget,
  hearedAt,
  description,
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  const body = JSON.stringify({
    firstname,
    lastname,
    email,
    country,
    budget,
    hearedAt,
    description,
  });
  console.log(body, "body")
  try {
    const res = await axios.post("/studio-contact-us", body, config);
    console.log(res.data);
    dispatch({
      type: CONTACT_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: CONTACT_FAIL
    });
  }
};