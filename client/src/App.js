/* eslint react/prop-types: 0 */
import React, { Fragment } from "react";
import { Route, Switch } from "react-router-dom"
import { connect } from "react-redux";
import DirectionProvider from 'react-with-direction/dist/DirectionProvider';
import HeaderComp from './components/Layouts/Header'
import FooterComp from './components/Layouts/Footer'
import Consultancy from './pages/Consultancy'
import Success from "./pages/Consultancy/success"
import setAuthToken from './utils/setAuthToken'

let layoutDirection;
if (localStorage.getItem('lng') === 'AR') {
  layoutDirection = 'rtl';
  // i18next.changeLanguage('AR')
}  else {
  layoutDirection = 'ltr';
  // i18next.changeLanguage('EN')
}
setAuthToken()
function App() {
  return (
    <Fragment>
      <DirectionProvider direction={layoutDirection}>
        < HeaderComp />
      </DirectionProvider>
      <Switch>
        <section className="container">
          <DirectionProvider direction={layoutDirection}>
            <Route path="/" component={Consultancy} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/success" component={Success} exact />
          </DirectionProvider>
        </section>
      </Switch>
      <DirectionProvider direction={layoutDirection}>
        <FooterComp />
      </DirectionProvider>
    </Fragment >
  )
}

export default connect(null, null)(App);
