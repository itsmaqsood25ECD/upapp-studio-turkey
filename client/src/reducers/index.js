import { combineReducers } from "redux";
import BlogManagement from './blog.management.reducer'

export default combineReducers({
    BlogManagement
});